module Main exposing (..)

import AnimationFrame
import Key exposing (..)
import Keyboard exposing (KeyCode)
import Platform
import Ports exposing (..)
import Task
import Time exposing (Time)
import Types exposing (..)
import Window exposing (Size)


-- MODEL


defaultModel : Types.Model
defaultModel =
    -- background image from http://server7.sky-map.org/imageView?image_id=905078
    -- set player coordinates to middle of bg image
    { imgSrc = "C10-LRGB-B1-70perc.jpg"
    , imgSize = Size 2797 1870
    , imgClipOffset = Coordinates 0 0
    , windowSize = Size 0 0
    , player = Coordinates 1398 935
    , velocity = Velocity 0 0
    }


init : ( Types.Model, Cmd Msg )
init =
    ( defaultModel
    , Task.perform InitSize (Window.size)
    )



-- UPDATE


type Msg
    = InitSize Window.Size
    | InitCanvas
    | Resize Window.Size
    | KeyDown KeyCode
    | TimeUpdate Time
    | ResizeCanvas
    | DrawCanvas
    | NoOp


update : Msg -> Types.Model -> ( Types.Model, Cmd Msg )
update msg model =
    case msg of
        InitSize size ->
            ( { model | windowSize = size }, Task.perform identity (Task.succeed InitCanvas) )

        -- Set image coordinates and draw initial canvas
        InitCanvas ->
            let
                -- set imgClip offsets based on player coordinates and window size
                clipOffsetX =
                    model.player.x - (toFloat (model.windowSize.width) / 2)

                clipOffsetY =
                    model.player.y - (toFloat (model.windowSize.height) / 2)

                clip_ =
                    Coordinates clipOffsetX clipOffsetY
            in
                ( { model | imgClipOffset = clip_ }
                , Ports.initCanvas ( model.imgSrc, clip_, model.windowSize )
                )

        Resize size ->
            ( { model | windowSize = size }, Task.perform identity (Task.succeed ResizeCanvas) )

        ResizeCanvas ->
            ( model
            , Cmd.batch
                [ Ports.resizeCanvas model
                , Task.perform identity (Task.succeed DrawCanvas)
                ]
            )

        DrawCanvas ->
            ( model, Ports.drawCanvas model )

        KeyDown keyCode ->
            ( keyDown keyCode model, Task.perform identity (Task.succeed DrawCanvas) )

        TimeUpdate dt ->
            ( applyPhysics dt model, Task.perform identity (Task.succeed DrawCanvas) )

        NoOp ->
            ( model, Cmd.none )


keyDown : KeyCode -> Model -> Model
keyDown keyCode model =
    case Key.fromCode keyCode of
        ArrowUp ->
            updateVelocity Vertical -0.25 model

        ArrowDown ->
            updateVelocity Vertical 0.25 model

        ArrowLeft ->
            updateVelocity Horizontal -0.25 model

        ArrowRight ->
            updateVelocity Horizontal 0.25 model

        OtherKey ->
            model


updateVelocity : Axis -> Float -> Model -> Model
updateVelocity axis newVelocity model =
    case axis of
        Vertical ->
            { model | velocity = Velocity 0 newVelocity }

        Horizontal ->
            { model | velocity = Velocity newVelocity 0 }


applyPhysics : Float -> Model -> Model
applyPhysics dt model =
    let
        maxClipOffsetX =
            model.imgSize.width - model.windowSize.width |> toFloat

        updatedClipOffsetX =
            model.imgClipOffset.x + model.velocity.horz * dt

        x_ =
            clamp 0 maxClipOffsetX updatedClipOffsetX

        maxClipOffsetY =
            model.imgSize.height - model.windowSize.height |> toFloat

        updatedClipOffsetY =
            model.imgClipOffset.y + model.velocity.vert * dt

        y_ =
            clamp 0 maxClipOffsetY updatedClipOffsetY
    in
        { model | imgClipOffset = Coordinates x_ y_ }



-- SUBSCRIPTIONS


subscriptions : Types.Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Window.resizes Resize
        , AnimationFrame.diffs TimeUpdate
        , Keyboard.downs KeyDown
        ]



-- MAIN


main : Program Never Types.Model Msg
main =
    Platform.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        }

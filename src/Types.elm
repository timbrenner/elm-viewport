module Types exposing (..)

import Window exposing (Size)


-- MODEL


type alias Coordinates =
    { x : Float, y : Float }


type alias Velocity =
    { horz : Float, vert : Float }


type Axis
    = Horizontal
    | Vertical


type alias Model =
    { imgSrc : String
    , imgSize : Size
    , imgClipOffset : Coordinates
    , windowSize : Size
    , player : Coordinates
    , velocity : Velocity
    }

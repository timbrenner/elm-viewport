port module Ports exposing (..)

import Types exposing (..)
import Window exposing (Size)


port initCanvas : ( String, Coordinates, Size ) -> Cmd msg


port resizeCanvas : Model -> Cmd msg


port drawCanvas : Model -> Cmd msg

# Viewport demo using canvas with Elm
This demo implements a game viewport using canvas through Elm ports & Elm 0.18.

Creating a game using canvas should be more performant than using svg.
The game play area in an online multi-player game is typically much, much larger than the visible window.
Updating only the viewport (the section of game being displayed) limits the portion of the model that needs updating during gameplay. 

Canvas support is limited in Elm at this time.  
The [elm-graphics](http://package.elm-lang.org/packages/evancz/elm-graphics/latest) library does not have a way to create a viewport with canvas.  
Basic game physics code is based on [elm-game-base](https://github.com/ohanhi/elm-game-base).

Use the arrow keys to move the viewport.  In this example, the background is created from an image.  No player object is shown.


## Demo at https://timbrenner.gitlab.io/elm-viewport/


### Development environment

Run [elm-live](https://github.com/tomekwi/elm-live) to compile source files automatically.

`$ elm-live src/Main.elm --output=public/main.js --open`

Open http://localhost:8000/public/index.html.  Refresh when source is updated.
